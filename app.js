// Jimi Toiviainen
// JSON web request server example

var express = require('express')
var app = express()
var bodyParser = require('body-parser')

app.use(express.static('public'))
// support parsing of application/json type post data
app.use(bodyParser.json())
//support parsing of application/x-www-form-urlencoded post data
app.use(bodyParser.urlencoded({ extended: true }))

var http = require('http').Server(app)
var port = process.env.PORT || 3000

// do database stuff instead of this
var data = 'this is default data'

app.get('/', function (req, res) {
    // do database stuff here and send that instead
    res.send(data)
})

app.post('/', function (req, res) {
    if (req.body.data){
        console.log(req.body.data)
        
        // do database stuff instead of this
        data = req.body.data
        res.send("ty for data")
        return
    }
    res.send("No req.body.data")
})

http.listen(port, function () {
    console.log('listening on *:' + port)
})