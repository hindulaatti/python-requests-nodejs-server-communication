# Jimi Toiviainen
# JSON web request client example

import requests
import traceback
import json

def getData(url):
	print()
	print('getting...')
	try:
		res = requests.get(url)

		print('response:',res.text)
	except:
		traceback.format_exc()
		
	print()

def sendData(data, url):
	print()
	print('sending...')
	data = {'data': data}
	headers = {'content-type': 'application/json'}
	print(url, data)
	try:
		res = requests.post(
			url=url,
			data=json.dumps(data),
			headers=headers
		)

		print('response:', res.text)
		
	except:
		traceback.format_exc()

	print()

############ PROGRAM ##############

url = "http://localhost:3000" # Where to send pic
data = {
	"asdf": "asdfg",
	"josf": "sdfg"
}
getData(url)
sendData(data, url)
getData(url)