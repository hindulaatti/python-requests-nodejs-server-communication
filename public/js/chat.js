
var username;
var color;
var COLORS = [
    '#e21400', '#91580f', '#f8a700', '#f78b00',
    '#58dc00', '#287b00', '#a8f07a', '#4ae8c4',
    '#3b88eb', '#3824aa', '#a700ff', '#d300e7'
];
var ict_date = Date(0);
var seppis_date = Date(0);

function getMsgDate(date){
	
    mo = ('0' + (date.getMonth() + 1)).slice(-2)
    dd = ('0' + date.getDate()).slice(-2)
    hh = ('0' + date.getHours()).slice(-2)
    mm = ('0' + date.getMinutes()).slice(-2)
    ss = ('0' + date.getSeconds()).slice(-2)
    return dd + '.' + mo + '. ' + hh + ':' + mm + ':' + ss
}

// Gets the color of a username through our hash function
const getUsernameColor = (username) => {
    // Compute hash code
    var hash = 7;
    for (var i = 0; i < username.length; i++) {
       hash = username.charCodeAt(i) + (hash << 5) - hash;
    }
    // Calculate color
    var index = Math.abs(hash % COLORS.length);
    return COLORS[index];
}

$(function () {        

    var socket = io();
    
    socket.emit('init');
    
    socket.on('ict_date', function (serverdate, coffee_level){
        ict_date = serverdate;
        $('#ict_date').text(ict_date.toString());
        $("#ict_coffee_level").text(coffee_level);
    })
    
    socket.on('seppis_date', function (serverdate, coffee_level){
        seppis_date = serverdate;
        $('#seppis_date').text(seppis_date.toString());
        $("#seppis_coffee_level").text(coffee_level);
    })

    $('#nick').submit(function () {
        username = $('#usernameInput').val();
        $('#loginpage').hide();
        $('#chatpage').show();
		document.getElementById('m').focus();
        return false;
    });

    $('#message').submit(function () {
        socket.emit('chat message', username, $('#m').val().trim());
        $('#m').val('');
        return false;
    });

    socket.on('chat message', function (nick, msg, msgdate) {
        
        msgdate = new Date(msgdate)
        var $date = $('<span class="date"/>')
            .text(getMsgDate(msgdate) + " ")
        var $usernameDiv = $('<span class="username"/>')
            .text(nick + ": ")
            .css('color', getUsernameColor(nick));
        var $messageBodyDiv = $('<span class="messageBody">')
            .text(msg);

        $('#messages').append($('<li>').append($date, $usernameDiv, $messageBodyDiv));
        $('.chat').scrollTop($('.chat')[0].scrollHeight);
    });

    socket.on('ict_send_picture', function (data) {
        $("#ict_imgid").attr("src","data:image/png;base64,"+data);
    });

    socket.on('seppis_send_picture', function (data) {
        $("#seppis_imgid").attr("src","data:image/png;base64,"+data);
    });
});